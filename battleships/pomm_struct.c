#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define N 10
#define WRITE 0
#define READ 1
#define INGAME 3
#define P1 1
#define P2 2
#define P1o 4
#define P2o 5
//V�rvide lubamiseks (windows.h teegi abil)
#define color_manipulation HANDLE hConsole;hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
#define color SetConsoleTextAttribute(hConsole, k);
#define color_conditions if(array[i][j] == 'O'){k=135;color;}\
if(array[i][j] == 'X'){k=140;color;}\
if(array[i][j] == 'S'){k=17;color;}\
if(array[i][j] == '8'){k=25;color;}
#define def_color k=7;color;

typedef struct{
    int id;
    char *nimi;
    char self_field[N][N];
    char opp_hit[N][N];
}strukt;
strukt Player1,Player2;

FILE *loe;
FILE *kirjuta;
FILE *gamedata;

strukt input(strukt playa);
void nameallo(char *Pbuf, char **Player);
void iostream(int IO, char array[N][N], int stuff);
void info(int IO, int turn);
int bombing();
void b_hit(char array_1[N][N], char array_2[N][N], char *Player);
int end(int turn, char array_1[N][N], char *Player);
void game();

int main(){
    Player1.id= 1;Player2.id= 2;
    if(fopen("gamedata.txt","r")==NULL){//esmak�ivitus
        gamedata=fopen("gamedata.txt","w");fclose(gamedata);
        kirjuta=fopen("p1_s.txt","w");fclose(kirjuta);
        kirjuta=fopen("p1_o.txt","w");fclose(kirjuta);
        kirjuta=fopen("p2_s.txt","w");fclose(kirjuta);
        kirjuta=fopen("p2_o.txt","w");fclose(kirjuta);
    }
    Player1=input(Player1);
    iostream(READ,Player1.self_field,P1);
    Player2=input(Player2);
    iostream(READ,Player2.self_field,P2);
    iostream(WRITE,Player1.opp_hit,P1o);
    iostream(WRITE,Player2.opp_hit,P2o);
    info(WRITE,INGAME);

    game();
    getch();
    return 0;
}

void nameallo(char *Pbuf, char **Player){
    Pbuf = (char*) malloc ((1024)*sizeof(char));
    gets(Pbuf);
    *Player= (char*) realloc (Pbuf,  ((strlen(Pbuf)+1)* sizeof(char)));
}

strukt input(strukt playa){
    int SC, SS,choice[6],Scx,Scy,count,valik,i,j,x,y,laev_in=0,random;
        char *buffer;
        system("cls");
        printf("Enter player name:\n");
        nameallo(buffer,&playa.nimi);
        free(buffer);
        for(y=0;y<10;y++){//v2ljade sisestus
            for(x=0;x<10;x++){
                playa.self_field[y][x]='S';
            }
        }
        for(y=0;y<10;y++){//v2ljade sisestus
            for(x=0;x<10;x++){
                playa.opp_hit[y][x]='S';
            }
        }
    puts("[ 1 ]\tRandom Field\n[ 2 ]\tEnter manually\n");
        switch(getch()){
            case'1':random=1;fflush(stdin);break;
            case'2':random=0;fflush(stdin);break;
            default:puts("By default, random");random=1;break;
        }
    for(SC=1,SS=4,count=0;SC<5;SC++){//ShipCounter \ ShipSize, ts�kliga paika pandud, mitu laeva sisestatakse ja kui pikad laevad sisestatakse.
        for(i=0;i<SC;i++,laev_in++){         //i -> v�limised ts�kklid | j-> kontrolli minits�kklid
            count=1;
            //shipplacing
            for(j=0;j<6;j++){
                choice[j]=0;
            }
            if(random==1){
                    x=rand()%11;
                    y=rand()%11;
            }else{
                printf("Enter %d. ships(%d size ship) coordinates:\n", laev_in+1,SS);
                if(random==0){iostream(READ,playa.self_field,INGAME);}
                do{
                    scanf("%d", &x);
                    x=x-1;
                }while(x < 0 || x > N);
                do{
                    scanf("%d", &y);
                    y=y-1;
                }while(y < 0 || y > N);
            }
            //KONTROLLID <<<<<<<<<<<<<<<<<<
            for(j=0,Scy=y;j<SS;j++,Scy++){//kontroll(paremale)
                if(playa.self_field[x][Scy]=='O' || Scy > N){break;}
                if(j==SS-1){
                    if(random==0){printf("%d- Right\n", count);}
                    choice[count]=1;count++;}
            }
            for(j=0,Scy=y;j<SS;j++,Scy--){ //kontroll(vasakule)
                if(playa.self_field[x][Scy]=='O' || Scy < 0){break;}
                if(j==SS-1){
                    if(random==0){printf("%d- Left\n", count);}
                    choice[count]=2;count++;}
            }
            for(j=0,Scx=x;j<SS;j++,Scx--){//kontroll(�les)
                if(playa.self_field[Scx][y]=='O' || Scx < 0){break;}
                if(j==SS-1){
                    if(random==0){printf("%d- Up\n", count);}
                    choice[count]=3;count++;}
            }
            for(j=0,Scx=x;j<SS;j++,Scx++){//kontroll(alla)
                if(playa.self_field[Scx][y]=='O' || Scx > N){break;}
                if(j==SS-1){
                    if(random==0){printf("%d- Down\n", count);}
                    choice[count]=4;count++;}
            }
            if(random==0){printf("%d- Cancel\n", count);}
            choice[count]=5;
            do{
            if(random==1){
                valik=rand()%(count+1);
            }
            else{
                scanf("%d", &valik);
            }
            }while(valik>count);
            //VALIKUD , tekivad vastavalt kontrollis v�imalikele v�imalustele (d�naamiline)
            if(choice[valik]==1){ //valik paremale
                for(j=0,Scy=y;j<SS;j++,Scy++){
                    playa.self_field[x][Scy]='O';
                }
            }
            if(choice[valik]==2){//valik vasakule
                for(j=0,Scy=y;j<SS;j++,Scy--){
                    playa.self_field[x][Scy]='O';
                }
            }
            if(choice[valik]==3){//valik �less
                for(j=0,Scx=x;j<SS;j++,Scx--){
                    playa.self_field[Scx][y]='O';
                }
            }
            if(choice[valik]==4){//valik alla
                for(j=0,Scx=x;j<SS;j++,Scx++){
                    playa.self_field[Scx][y]='O';
                }
            }
            if(choice[valik]==5){i-=1;laev_in-=1;}//valik(cancel)
            if(random==1){system("cls");}
        }
        SS=SS-1;
    }
            if(playa.id==Player1.id){
                iostream(WRITE,playa.self_field,P1);
            }
            if(playa.id==Player2.id){
                iostream(WRITE,playa.self_field,P2);
            }
            system("cls");
            return playa;

}

void iostream(int IO, char array[N][N], int stuff){
    int i,j,k;           //0 - failikirjutus | 1- failist lugemine
    if(IO==WRITE){
        if(stuff==P1){kirjuta=fopen("p1_s.txt","w");}
        if(stuff==P2){kirjuta=fopen("p2_s.txt","w");}
        if(stuff==P1o){kirjuta=fopen("p1_o.txt","w");}
        if(stuff==P2o){kirjuta=fopen("p2_o.txt","w");}
        for(i=0;i<N;i++){
            for(j=0;j<N;j++){
                fprintf(kirjuta,"%c", array[i][j]);
            }
        }
        fclose(kirjuta);
    }else{
        if(IO==READ){
            if(stuff==P1){loe=fopen("p1_s.txt","r");}
            if(stuff==P2){loe=fopen("p2_s.txt","r");}
            if(stuff==P1o){loe=fopen("p1_o.txt","r");}
            if(stuff==P2o){loe=fopen("p2_o.txt","r");}
        }
        color_manipulation;
        def_color;
        for(i=0;i<N;i++){
            for(j=0;j<N;j++){
                def_color;
                color_conditions;
                if(IO==READ){
                        fscanf(loe,"%c", &array[i][j]);
                }
                printf("%c%c", array[i][j], array[i][j]);
            }
            printf("\n");
            def_color;
        }
        fclose(loe);
    }
}

void game(){
printf("Game begins!\n");
system("start pomm_struct_client.exe");
    do{
        printf("%s , Your field:\n", Player1.nimi);
        iostream(READ,Player1.self_field,P1);
        printf("%s bombings:\n", Player2.nimi);
        iostream(READ,Player1.opp_hit,P1o);
        printf("waiting for %s to make a move...\n", Player2.nimi);
        info(READ,P1);
        end(P2,Player2.opp_hit,Player2.nimi);
        system("cls");
    printf("%s's turn:\n", Player1.nimi);//Player 1 k�ik
    iostream(READ,Player1.self_field,P1);
    printf("%s bombings:\n", Player2.nimi);
    iostream(READ,Player1.opp_hit,P1o);
    b_hit(Player2.self_field,Player1.opp_hit,Player1.nimi);
    iostream(WRITE,Player2.self_field,P2);
    iostream(WRITE,Player1.opp_hit,P1o);
    system("cls");
    printf("%s's field:\n", Player1.nimi);
    iostream(READ,Player1.self_field,P1);
    printf("%s (un)successful bombings:\n",Player2.nimi);
    iostream(READ,Player1.opp_hit,P1o);
        gamedata=fopen("gamedata.txt","w");
        fprintf(gamedata, "2");
        fclose(gamedata);
        system("cls");
    }while(1);
}

void info(int IO, int turn){
    if(IO==0){
        kirjuta=fopen("gamedata.txt","w");
        fprintf(kirjuta,"%s\n%s\n1\n", Player1.nimi, Player2.nimi);
        fclose(kirjuta);
    }else{
        int k2ik,lc=0;
        while(k2ik!=turn){
            loe=fopen("gamedata.txt","r");
            fscanf(loe,"%d", &k2ik);
            fclose(loe);
            if(k2ik==9){break;}
            Sleep(500);
            lc++;
        }
    }
}

int bombing(){
    int coordinate;
    do{
        scanf("%d", &coordinate);
    }while(coordinate < 0 || coordinate > 10);
    return coordinate-1;
}

void b_hit(char array_1[N][N], char array_2[N][N], char *Player){
    int x,y;
    printf("Enter bombing coordinates, separate by space (e.g. 1 1)\n");
        x=bombing();
        y=bombing();
    if(array_1[x][y]=='X'){
        while(array_1[x][y]=='X'){
            printf("AGAIN!\n");
            x=bombing();
            y=bombing();
        }
    }
    if(array_1[x][y]=='8'){
        while(array_1[x][y]=='8'){
            printf("AGAIN!\n");
            x=bombing();
            y=bombing();
        }
    }
    if(array_1[x][y] == 'O'){
        array_1[x][y]='X';
        array_2[x][y]='X';
        printf("\a");
    }else{
        array_1[x][y]='8';
        array_2[x][y]='8';
    }
    end(P1,array_1,Player);
}

int end(int turn, char array_1[N][N], char *Player){
    int i,j,c=0;
    if(turn==P2){
        iostream(READ,array_1,INGAME);
    }
    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            if(array_1[i][j] == 'O'){
                c=1;
                i=N;
                break;
            }
        }
    }
            if(c==0){
            printf("%s won!\n", Player);
            FILE *winner=fopen("winner.vbs","w");
            fprintf(winner,"CreateObject(\"SAPI.SpVoice\").Speak\"%s Has won the game!\"",Player);
            fclose(winner);
            system("winner.vbs");
            free(Player1.nimi);
            free(Player2.nimi);
            printf("Press any key to exit!\n");
            getch();
            exit(0);
        }
}

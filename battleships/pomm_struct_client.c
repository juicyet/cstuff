#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define N 10
#define WRITE 0
#define READ 1
#define INGAME 3
#define P1 1
#define P2 2
#define P1o 4
#define P2o 5
#define SISSE 6

#define color_manipulation HANDLE hConsole;hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
#define color SetConsoleTextAttribute(hConsole, k);
#define color_conditions if(array[i][j] == 'O'){k=135;color;}\
if(array[i][j] == 'X'){k=140;color;}\
if(array[i][j] == 'S'){k=17;color;}\
if(array[i][j] == '8'){k=25;color;}
#define def_color k=7;color;

typedef struct{
    int id;
    char *nimi;
    char self_field[N][N];
    char opp_hit[N][N];
}strukt;
strukt Player1,Player2;

FILE *loe;
FILE *kirjuta;
FILE *gamedata;

strukt input(strukt playa);
void nameallo(char *Pbuf, char **Player);
void iostream(int IO, char array[N][N], int stuff);
void info(int IO, int turn);
int bombing();
void b_hit(char array_1[N][N], char array_2[N][N], char *Player);
int end(int turn, char array_1[N][N], char *Player);
void game();

int main(){
    Player1.id= 1;Player2.id= 2;
    gamedata=fopen("gamedata.txt", "r");
    Player1=input(Player1);
    Player2=input(Player2);
    fclose(gamedata);
    game();
    getch();
    return 0;
}

void nameallo(char *Pbuf, char **Player){
    Pbuf = (char*) malloc ((1024)*sizeof(char));
    fscanf(gamedata,"%s", Pbuf);
    *Player= (char*) realloc (Pbuf,  ((strlen(Pbuf)+1)* sizeof(char)));
}

strukt input(strukt playa){
    char *buffer;
    nameallo(buffer,&playa.nimi);
    free(buffer);
    return playa;
}

void iostream(int IO, char array[N][N], int stuff){
    int i,j,k;
    if(IO==WRITE){
        if(stuff==P1){kirjuta=fopen("p1_s.txt","w");}
        if(stuff==P2){kirjuta=fopen("p2_s.txt","w");}
        if(stuff==P1o){kirjuta=fopen("p1_o.txt","w");}
        if(stuff==P2o){kirjuta=fopen("p2_o.txt","w");}
           //0 - failikirjutus | 1- failist lugemine
        for(i=0;i<N;i++){
            for(j=0;j<N;j++){
                fprintf(kirjuta,"%c", array[i][j]);
            }
        }
        fclose(kirjuta);
    }if(IO==SISSE){
        if(stuff==P1){loe=fopen("p1_s.txt","r");}
        if(stuff==P2){loe=fopen("p2_s.txt","r");}
        if(stuff==P1o){loe=fopen("p1_o.txt","r");}
        if(stuff==P2o){loe=fopen("p2_o.txt","r");}
            for(i=0;i<N;i++){
                for(j=0;j<N;j++){
                        fscanf(loe,"%c", &array[i][j]);
                }
            }
        fclose(loe);
    }else{
        if(IO==READ){
            if(stuff==P1){loe=fopen("p1_s.txt","r");}
            if(stuff==P2){loe=fopen("p2_s.txt","r");}
            if(stuff==P1o){loe=fopen("p1_o.txt","r");}
            if(stuff==P2o){loe=fopen("p2_o.txt","r");}

        }
        color_manipulation;
        def_color;
        for(i=0;i<N;i++){
            for(j=0;j<N;j++){

                if(IO==READ){
                    fscanf(loe,"%c", &array[i][j]);
                }
                def_color;
                color_conditions;
                printf("%c%c", array[i][j], array[i][j]);
            }
            printf("\n");
            def_color;
        }
        fclose(loe);
    }
}

void game(){
printf("Game begins!\nGet Ready! %s\t&\t%s!\n", Player1.nimi,Player2.nimi);
    printf("%s , Your field:\n", Player2.nimi);
    iostream(READ,Player2.self_field,P2);
    printf("%s bombings:\n", Player1.nimi);
    iostream(READ,Player2.opp_hit,P2o);
    info(INGAME,P2);
    do{
        printf("%s , Your field:\n", Player2.nimi);
        iostream(READ,Player2.self_field,P2);
        printf("%s bombings:\n", Player1.nimi);
        iostream(READ,Player2.opp_hit,P2o);
        printf("waiting for %s to make a move...\n", Player1.nimi);
        info(READ,P2);
        iostream(SISSE,Player1.self_field,P1);
        end(P1,Player1.self_field,Player1.nimi);//changed stuff here
        system("cls");
    printf("%s's turn:\n", Player2.nimi);//Player 2 k�ik
    iostream(READ,Player2.self_field,P2);
    printf("%s bombings:\n", Player1.nimi);
    iostream(READ,Player2.opp_hit,P2o);
    b_hit(Player1.self_field,Player2.opp_hit,Player2.nimi);
    iostream(WRITE,Player1.self_field,P1);
    iostream(WRITE,Player2.opp_hit,P2o);
    system("cls");
    printf("%s's field:\n", Player2.nimi);
    iostream(READ,Player2.self_field,P2);
    printf("%s (un)successful bombings:\n",Player1.nimi);
    iostream(READ,Player2.opp_hit,P2o);
        gamedata=fopen("gamedata.txt","w");
        fprintf(gamedata, "1");
        fclose(gamedata);
        system("cls");
    }while(1);
}

void info(int IO, int turn){
    int k2ik,lc=0,tmp;
    char rida[128];

    if(IO==3){
        gamedata=fopen("gamedata.txt","w");
        fprintf(gamedata, "1");
        fclose(gamedata);
    }
        while(k2ik!=turn){

            loe=fopen("gamedata.txt","r");
            fscanf(loe,"%d", &k2ik);
            fclose(loe);
            if(k2ik==9){break;}
            Sleep(500);
            lc++;
        }

}

int bombing(){
    int coordinate;
    do{
        scanf("%d", &coordinate);
    }while(coordinate < 0 || coordinate > 10);
    return coordinate-1;
}

void b_hit(char array_1[N][N], char array_2[N][N], char *Player){
    int x,y;
    printf("Enter bombing coordinates, separate by space (e.g. 1 1)\n");
        x=bombing();
        y=bombing();
    if(array_1[x][y]=='X'){
        while(array_1[x][y]=='X'){
            printf("AGAIN!\n");
            x=bombing();
            y=bombing();
        }
    }
    if(array_1[x][y]=='8'){
        while(array_1[x][y]=='8'){
            printf("AGAIN!\n");
            x=bombing();
            y=bombing();
        }
    }
    if(array_1[x][y] == 'O'){
        array_1[x][y]='X';
        array_2[x][y]='X';
        printf("\a");
    }else{
        array_1[x][y]='8';
        array_2[x][y]='8';
    }
    end(P2,array_1,Player);
}

int end(int turn, char array_1[N][N], char *Player){
    int i,j,c=0;
    if(turn==P1){
        iostream(READ,array_1,INGAME);
    }
    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            if(array_1[i][j] == 'O'){
                c=1;
                i=N;
                break;
            }
        }
    }
            if(c==0){
            printf("%s won!\n", Player);
            FILE *winner=fopen("winner.vbs","w");
            fprintf(winner,"CreateObject(\"SAPI.SpVoice\").Speak\"%s Has won the game!\"",Player);
            fclose(winner);
            system("winner.vbs");
            free(Player1.nimi);
            free(Player2.nimi);
            gamedata=fopen("gamedata.txt", "w");
            fprintf(gamedata, "9");
            fclose(gamedata);
            printf("Press any key to exit!\n");
            getch();
            exit(0);
        }
}

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>

  typedef struct {
      int paev; 
      int kuu;
      int aasta;
  } kuupaev;

  typedef struct {
      char kood[5];
      char nimetus[40];
      int kogus;
      int hind;
      kuupaev *kuupaevad;
  } ese;

	typedef struct{
		int visit_c0;
		int visit_c1;
		int visit_c2;
	} history;
  
int filecheck();
ese readData(ese *esemed);
void kuvamine(ese *esemed);
void sorteering(ese *esemed);
void search(ese *esemed);
void history_display(history history_log);

int main(){
    int N, i,x; // N n2itab mitu rida on sisendis
    N=filecheck();
	
    ese *esemed;
	history history_log;
	kuupaev *kuupaevad;
	
    esemed=(ese*)malloc(N*sizeof(ese));    
	kuupaevad=(kuupaev*)malloc(N*sizeof(kuupaev));
	
	history_log.visit_c0 = 0;
	history_log.visit_c1 = 0;
	history_log.visit_c2 = 0;
	
	i=0;
	while(i<N){
		(esemed+i)->kuupaevad = (kuupaev*)malloc(N*sizeof(kuupaev));
		i++;
    }    
	
    readData(esemed);  
	
	i=0;
	printf("Warehouse.\n\nPress any key to continue.\n");
	getchar();
	while(1)
	{		
		system("cls");
		printf("Menu:\n0 - show products\n1 - sort product list\n2 - product search\n3 - history\n4 - Exit\nInput:");
		scanf("%d", &x);
		switch(x)
		{
			case 0:
				{
					history_log.visit_c0++;
					kuvamine(esemed);
					break;
				}
			case 1:
				{
					history_log.visit_c1++;
					sorteering(esemed);
					break;
				}
			case 2:
				{
					history_log.visit_c2++;
					search(esemed);
					break;
				}
			case 3:
				{
					history_display(history_log);					
					break;
				}
			case 4:
				{
					exit(0);
					break;
				}	
		}
	    getchar();
		getchar();
		i++;   
    }
	
    free(esemed);
    free(kuupaevad);
	
    getchar();
    getchar();
	
    return 0;
}

//Ajaloo kuvamine
void history_display(history history_log){
	printf("%d times displayed, %d times sorted, %d times searched during current session.\n", history_log.visit_c0++,history_log.visit_c1++,history_log.visit_c2++);
}

//Otsing
void search(ese *esemed){
	int N,i,x, k1,k2,k3;
	char k[40];
	N=filecheck();
	
	printf("\nSearch\ncode - 0\nname - 1\ndate - 2\nprice - 3\nInput: ");
	scanf("%d", &x);
	
	switch(x)
	{
	case 0:
		{
		printf("\nYou chose code search.\n");
		printf("Enter code: ");
		scanf("%s", k);
		printf("\nProducts including code: %s :\n\n", k);
		i=0;
		while(i<N)
			{
			if (strstr((esemed+i)->kood, k)  != 0)
				{
					printf("%s  <%s>**<AMOUNT: %d>**<PRICE: %d Eur>**<DATE: %d.%d.%d>\n\n", (esemed+i)->kood, (esemed+i)->nimetus, (esemed+i)->kogus, (esemed+i)->hind, (esemed+i)->kuupaevad->paev, (esemed+i)->kuupaevad->kuu, (esemed+i)->kuupaevad->aasta);
				}
				i++;
			}
		break;
		}
	case 1:
		{
		printf("\nYou chose name search.\n");
		printf("Enter name: ");
		scanf("%s", k);
		printf("\nProducts including name: %s :\n\n", k);
		i=0;
		while(i<N)
			{
			if (strstr((esemed+i)->nimetus, k)  != 0)
				{
					printf("%s  <%s>**<AMOUNT: %d>**<PRICE: %d Eur>**<DATE: %d.%d.%d>\n\n", (esemed+i)->kood, (esemed+i)->nimetus, (esemed+i)->kogus, (esemed+i)->hind, (esemed+i)->kuupaevad->paev, (esemed+i)->kuupaevad->kuu, (esemed+i)->kuupaevad->aasta);
				}
				i++;
			}
		break;
		}
	case 2:
		{
		printf("\nYou chose date search.\n");
		printf("Enter date\nIf you enter only the year, month or day correctly, only proper input will be considered.\nFORMAT:(dd mm yyyy) - separate with space: ");
		scanf("%d %d %d", &k1, &k2, &k3);
		printf("\nDate search result:\n\n", k1,k2,k3);
		i=0;
		while(i<N)
			{
			if ((esemed+i)->kuupaevad->paev == k1 || (esemed+i)->kuupaevad->kuu == k2 || (esemed+i)->kuupaevad->aasta == k3)
				{
					printf("%s  <%s>**<AMOUNT: %d>**<PRICE: %d Eur>**<DATE: %d.%d.%d>\n\n", (esemed+i)->kood, (esemed+i)->nimetus, (esemed+i)->kogus, (esemed+i)->hind, (esemed+i)->kuupaevad->paev, (esemed+i)->kuupaevad->kuu, (esemed+i)->kuupaevad->aasta);
				}
				i++;
			}
		break;
		}
	case 3:
		{
		printf("\nYou chose price search.\n");
		printf("Enter price: ");
		scanf("%d", &k1);
		printf("\nProducts costing %d euros and less:\n\n", k1);
		i=0;
		while(i<N)
			{
			if ((esemed+i)->hind <= k1)
				{
					printf("%s  <%s>**<AMOUNT: %d>**<PRICE: %d Eur>**<DATE: %d.%d.%d>\n\n", (esemed+i)->kood, (esemed+i)->nimetus, (esemed+i)->kogus, (esemed+i)->hind, (esemed+i)->kuupaevad->paev, (esemed+i)->kuupaevad->kuu, (esemed+i)->kuupaevad->aasta);
				}
				i++;
			}
		break;
		} 	
	}
}

//Kuvamine
void kuvamine(ese *esemed){     
	int N,i;
	N=filecheck();
	
	printf("\nNEW OUTPUT:\n\nID#\n\n");
	
	i=0;	
    while(i<N)
	{
		printf("%s  <%s>**<AMOUNT: %d>**<PRICE: %d Eur>**<DATE: %d.%d.%d>\n\n", (esemed+i)->kood, (esemed+i)->nimetus, (esemed+i)->kogus, (esemed+i)->hind, (esemed+i)->kuupaevad->paev, (esemed+i)->kuupaevad->kuu, (esemed+i)->kuupaevad->aasta);
		i++;
    }   
}

//Sorteerimine, m6lemat pidi
void sorteering(ese *esemed){
	int N,i,j,x;
	N=filecheck();
	
	ese *abi;
	abi=(ese*)malloc(N*sizeof(ese));	
	
	printf("\nDo you wish to sort in descending (0) or ascending (1) order?\nInput: ");
	scanf("%d", &x);

	switch(x)
	{
	case 0:
		{	
			for (j=0;j<N-1;j++)
			{
				for (i=0;i<N-1;i++)
				{	    
				   if (strcmp((esemed+i)->kood, (esemed+i+1)->kood) > 0)
						{
							 *abi = *(esemed+i);
								 
							 *(esemed+i) = *(esemed+i+1);
						  
							 *(esemed+i+1) = *abi;
						}
				}
			}
			break;
		}	
	case 1:
		{
			for (j=0;j<N-1;j++)
			{
				for (i=0;i<N-1;i++)
				{	    
				   if (strcmp((esemed+i)->kood, (esemed+i+1)->kood) < 0)
						{
							 *abi = *(esemed+i);
								 
							 *(esemed+i) = *(esemed+i+1);
						  
							 *(esemed+i+1) = *abi;
						}
				}
			}
		
		}
		break;	
	}
    free(abi);
}

//faili checkker
int filecheck(){ 
    FILE *sisend;
    char abi2[20], abi3[20];
    int abi4, abi5, i, N, k1, k2, k3;
	
    i=0;
	
    if ( (sisend = fopen ("sisend.txt", "r") ) == NULL )//kontroll
		{
		   printf ("File does not exist or inacessible!\n");      
		   getchar();
		   exit(0);
		}    
    else
		{
		 while(!feof(sisend))
			{ 
			
				fscanf(sisend, "%s %s %d %d %d %d %d", abi2, abi3, &abi4, &abi5, &k1, &k2, &k3);             
				i++;
			}
		fclose(sisend);
		}
    N=i; 
    return N;    
}

//Loeb andmed sisse
ese readData(ese *esemed){
    int N,i;   
    N=filecheck();  
    i=0;
    FILE* sisend;
    if ( (sisend = fopen ("sisend.txt", "r") ) == NULL )//sisselugemine
		{
			printf ("File does not exist or inacessible!\n");      
			getchar();
			exit(0);
		}
    else
		{
		 while(i<N)
			{ 

				fscanf(sisend, "%s %s %d %d %d %d %d", (esemed+i)->kood, (esemed+i)->nimetus, &(esemed+i)->kogus, &(esemed+i)->hind, &(esemed+i)->kuupaevad->paev, &(esemed+i)->kuupaevad->kuu, &(esemed+i)->kuupaevad->aasta);             
				i++;
			}
		}
    fclose(sisend);
    
    return *esemed;
}





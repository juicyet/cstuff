#include <stdio.h>
#include <time.h>
#include <string.h>

#define f1 10
#define t 20
#define N 10
#define R 15

/*v2ljastab s6idus osalevate pilootide nimed*/
void piloodid_ekraanile(char piloodid[][t], int pilootide_arv){

    int i;
    printf("Pilots:\n");
    for(i=0; i<pilootide_arv; i++) printf("%s ", piloodid[i]);
    }

/*igale s6itjale yks random ringi aeg*/
void yhe_ringi_ajad(int ringi_ajad[], int pilootide_arv){

    int i;
    for(i=0; i<pilootide_arv; i++) ringi_ajad[i] = aeg();
    }

/*random aja genereerimine*/
int aeg(){

    return rand() % 35 + 75;
    }

/*piloodi nime ja ringi aja v2ljastamine*/
void piloodid_ja_ajad_ekraanile(char piloodid[][t], int ringi_ajad[], int pilootide_arv){

    int i;
    for(i=0; i<pilootide_arv; i++) printf("\n%20s, %5d", piloodid[i], ringi_ajad[i]);
    }

/*kiireima piloodi leidmine*/
int kiireim_piloot(int ringi_ajad[], int pilootide_arv){

    int i, max = ringi_ajad[0], maxi;
    for(i=1; i<pilootide_arv; i++)
        if(ringi_ajad[i] < max){
            max = ringi_ajad[i];
            maxi = i;
            }
    return maxi;
    }

/*aeglaseima piloodid leidmine*/
int aeglaseim_piloot(int ringi_ajad[], int pilootide_arv){

    int i, min = ringi_ajad[0], mini;
    for(i=1; i<pilootide_arv; i++)
        if(ringi_ajad[i] > min){
            min = ringi_ajad[i];
            mini = i;
            }
    return mini;
    }

/*kiireima ja aeglaseima piloodi v2ljastus*/
void piloot_ekraanile(char piloodid[][t], int ringi_ajad[], int kiireimi, int aeglaseimi){

    printf("\nFastest pilot %s, laptime %d s", piloodid[kiireimi], ringi_ajad[kiireimi]);
    printf("\nSlowest pilot %s, laptime %d s", piloodid[aeglaseimi], ringi_ajad[aeglaseimi]);
    }

/*leiab keskmise aja ja v2ljastab keskmisest kiiremad ja aeglasemad piloodid*/
void kiired_ja_vihased(char piloodid[][t], int pilootide_arv, int ringi_ajad[]){

    int i, summa=0;
    double keskmine=0;
    for(i=0; i<pilootide_arv; i++) summa += ringi_ajad[i];
    keskmine = summa / 10;
    printf("\nAverage laptime %-2.0lf s", keskmine);
    /*kiiremad kui keskmine*/
    printf("\nFaster than average");
    for(i=0; i<pilootide_arv; i++) if(ringi_ajad[i] < keskmine) printf("\n%20s %5d", piloodid[i], ringi_ajad[i]);
    /*aeglasemad kui keskmine*/
    printf("\nSlower than average");
    for(i=0; i<pilootide_arv; i++) if(ringi_ajad[i] > keskmine) printf("\n%20s %5d", piloodid[i], ringi_ajad[i]);
    }

/*leiab keskmise aja ja koostab pilootidest ja ringi aegades 2 uut massiivi*/
void kiired_ja_vihased2(char piloodid[][t], int pilootide_arv, int ringi_ajad[], char pkiired[][t], int rkiired[], int *kiired, char paeglased[][t], int raeglased[], int *aeglased){

    int i, summa=0, x=0, y=0;
    double keskmine=0;
    for(i=0; i<pilootide_arv; i++) summa += ringi_ajad[i];
    keskmine = summa / 10;
    printf("\nAverage laptime %-2.0lf s", keskmine);
    /*kiiremad ja aeglasemad kui keskmine*/
    for(i=0; i<pilootide_arv; i++){
        if(ringi_ajad[i] < keskmine){
            strcpy(pkiired[x], piloodid[i]);
            rkiired[x] = ringi_ajad[i];
            x++;
            }
        if(ringi_ajad[i] > keskmine){
            strcpy(paeglased[y], piloodid[i]);
            raeglased[y] = ringi_ajad[i];
            y++;
            }
        }
    *kiired = x;
    *aeglased = y;
    }

/*keskmisest kiiremate ja aeglasemate v2ljastamine*/
void p_ekraanile(char pkiired[][t], int rkiired[], int kiired, char paeglased[][t], int raeglased[], int aeglased){

    int i;
    printf("\nFaster than average");
    for(i=0; i<kiired; i++) printf("\n%20s %5d", pkiired[i], rkiired[i]);
    printf("\nSlower than average");
    for(i=0; i<aeglased; i++) printf("\n%20s %5d", paeglased[i], raeglased[i]);
    }

/*kysib kasutaja k2est mitu ringi s6idetakse max 15*/
int ringide_arv(){

    int laps;
    do{
        printf("\nNumber of laps (1-15): ");
        scanf("%d", &laps);
        }while(laps < 1 || laps > 15);
    return laps;
    }

/*random ringiajad s6itjatele vastavalt ringide arvule*/
void lap_times(int pilootide_arv, int ringid, int laptimes[][R], int sum[]){

    int i, j;
    for(i=0; i<pilootide_arv; i++)
        for(j=0; j<ringid; j++){
            laptimes[i][j] = aeg();
            sum[i] += laptimes[i][j];
            }
    }

/*v2ljastab tabeli pilootide nimede, ringide ja ringide summaga*/
void vomit(int pilootide_arv, int ringid, int laptimes[][R], char piloodid[][t], int sum[]){

    int i, j;
    for(i=0; i<pilootide_arv; i++){
        printf("\n%20s ", piloodid[i]);
        for(j=0; j<ringid; j++) printf("%4d", laptimes[i][j]);
        printf(" |%6ds", sum[i]);
        }
    }

/*kiireimad ja aeglaseimad ringid*/
void fastest_slowest_lap(char piloodid[][t], int pilootide_arv, int laptimes[][R], int ringid){

    int i, j, max[R], min[R], maxi[R], mini[R], ringikiireim[R], ringikiireimi[R], x, ringiaeglaseim[R], ringiaeglaseimi[R], y;
    /*leiab ja v2ljastab iga piloodi kiireima ja aeglaseima ringi*/
    for(i=0; i<pilootide_arv; i++){
        max[i] = laptimes[i][0];
        min[i] = laptimes[i][0];
        maxi[i] = 0;
        mini[i] = 0;
        for(j=1; j<ringid; j++){
            if(laptimes[i][j] < max[i]){
                max[i] = laptimes[i][j];
                maxi[i] = j;
                }
            if(laptimes[i][j] > min[i]){
                min[i] = laptimes[i][j];
                mini[i] = j;
                }
            }
        }
    for(i=0; i<pilootide_arv; i++) printf("\n%15s fastest lap - nr.%2d (%3ds), slowest lap - nr.%2d (%3ds)", piloodid[i], maxi[i]+1, max[i], mini[i]+1, min[i]);
    printf("\n\n");
    /*leiab ja v2ljastab iga ringi kiireima ja aeglaseima piloodi*/
    for(j=0; j<ringid; j++){
        ringikiireim[j] = laptimes[0][j];
        ringikiireimi[j] = 0;
        ringiaeglaseim[j] = laptimes[0][j];
        ringiaeglaseimi[j] = 0;
        for(i=0; i<pilootide_arv; i++){
            if(laptimes[i][j] < ringikiireim[j]){
                ringikiireim[j] = laptimes[i][j];
                ringikiireimi[j] = i;
                }
            if(laptimes[i][j] > ringiaeglaseim[j]){
                ringiaeglaseim[j] = laptimes[i][j];
                ringiaeglaseimi[j] = i;
                }
            }
        }
    for(i=0; i<ringid; i++){
        x = ringikiireimi[i];
        y = ringiaeglaseimi[i];
        printf("\n%d. lap's fastest was %s with laptime %ds", i+1, piloodid[x], ringikiireim[i]);
        printf("\n%d. lap's slowest was %s with laptime %ds\n", i+1, piloodid[y], ringiaeglaseim[i]);
        }
    }

/*leiab v6itja ja v2ljastab*/
void v6itja(char piloodid[][t], int pilootide_arv, int sum[]){

    int i, vsum, sek, min, v6itja;
    vsum = sum[0];
    for(i=0; i<pilootide_arv; i++)
        if(sum[i] < vsum){
            vsum = sum[i];
            v6itja = i;
            }
    sek = vsum % 60;
    min = vsum / 60;
    printf("\n\nThe winner is %s with laptime %d min and %d sec (%dsec)\nw00t!", piloodid[v6itja], min, sek, sum[v6itja]);
    }

/*peaprogramm*/
int main(void){

    /*deklaratsioon*/
    char piloodid[][t] = {"F.Alonso", "M.Webber", "L.Hamilton", "S.Vettel", "J.Button",
                        "F.Massa", "R.Kubica", "N.Rosberg", "M.Schumacher", "R.Barrichello"};
    char pkiired[f1][t], paeglased[f1][t];
    int ringi_ajad[N], pilootide_arv = N, kiireimi, aeglaseimi, rkiired[N], kiired, raeglased[N], aeglased;
    int ringid, laptimes[f1][R];
    int sum[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    /*random aeg*/
    srand(time(NULL));

    /*alamprogrammid*/
    piloodid_ekraanile(piloodid, pilootide_arv);
    printf("\n");
    yhe_ringi_ajad(ringi_ajad, pilootide_arv);
    piloodid_ja_ajad_ekraanile(piloodid, ringi_ajad, pilootide_arv);
    printf("\n");
    kiireimi = kiireim_piloot(ringi_ajad, pilootide_arv);
    aeglaseimi = aeglaseim_piloot(ringi_ajad, pilootide_arv);
    piloot_ekraanile(piloodid, ringi_ajad, kiireimi, aeglaseimi);
    printf("\n");
    kiired_ja_vihased(piloodid, pilootide_arv, ringi_ajad);
    printf("\n");
    kiired_ja_vihased2(piloodid, pilootide_arv, ringi_ajad, pkiired, rkiired, &kiired, paeglased, raeglased, &aeglased);
    p_ekraanile(pkiired, rkiired, kiired, paeglased, raeglased, aeglased);
    printf("\n");

    /*kolmas ylesanne*/
    ringid = ringide_arv();
    lap_times(pilootide_arv, ringid, laptimes, sum);
    vomit(pilootide_arv, ringid, laptimes, piloodid, sum);
    printf("\n");
    fastest_slowest_lap(piloodid, pilootide_arv, laptimes, ringid);
    v6itja(piloodid, pilootide_arv, sum);
    getchar();
    getchar();
    printf("\n\n");
    return 0;
    }
